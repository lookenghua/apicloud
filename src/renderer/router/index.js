import Vue from 'vue';
import Router from 'vue-router';
import index from '@/components/index';
import jsonFormat from '@/components/jsonFormat'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path:'/jsonFormat',
      name:'jsonFormat',
      component: jsonFormat
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
});
