const APICloud = require('apicloud-tools-core');
const path = require('path');
let autoUpdateDir = null;
const chokidar = require('chokidar');
let watcher;

export function startWifi(port) {
  APICloud.startWifi({port: port || 10915});
}

export function endWifi() {
  APICloud.endWifi({});
}

export function syncWifi(path, updateAll) {
  APICloud.syncWifi({
    projectPath: path,
    syncAll: updateAll
  });
}

export function autoUpdate(dir) {
  if (autoUpdateDir != dir) {
    if(watcher){
      watcher.close();
    }
  }
  autoUpdateDir = dir;
  watcher = chokidar.watch([dir], {
    persistent: true,
    ignored: path.join(dir, '\\.svn'),
    ignoreInitial: false,
    followSymlinks: true,
    disableGlobbing: true,
    usePolling: false,
    interval: 1000,
    binaryInterval: 300,
    alwaysStat: false,
    depth: 10,
    awaitWriteFinish: {
      stabilityThreshold: 2000,
      pollInterval: 100
    },

    ignorePermissionErrors: false
  });

  watcher
    .on('error', error => {
    })
    .on('ready', () => {
    })
    .on('raw', () => {
      syncWifi(dir, false);
    });
}

export function getWifiInfo() {
  return APICloud.wifiInfo();
}

export function logInfo(callback) {
  return new Promise(resolve => {
    APICloud.wifiLog(callback);
    resolve();
  });
}
