import { app, BrowserWindow, ipcMain } from 'electron';
import { initMenu } from './Menu';
import { initTray } from './Tray';
import { startWifi, endWifi, syncWifi, autoUpdate, logInfo } from './apicloud';
/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\');
}

let appIcon;
let mainWindow;
let newPageWin = {
  jsonEditorWin: null
};

const winURL = process.env.NODE_ENV === 'development' ? `http://localhost:9080` : `file://${__dirname}/index.html`;

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 563,
    minWidth: 348,
    useContentSize: true,
    show: false
  });
  mainWindow.loadURL(winURL);
  
  initMenu(mainWindow);
  initTray(mainWindow);
  
  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });
  mainWindow.on('closed', () => {
    appIcon = null;
    mainWindow = null;
  });
  
  startWifi();
  logInfo(function (log) {
    mainWindow.webContents.send('all-in', {
      type: 2,
      args: log
    });
  });
  
  ipcMain.on('apicloud', function (e, args) {
    if (args.type == 1) {
      syncWifi(args.path, args.isFull);
    } else if (args.type == 2) {
      autoUpdate(args.path);
    }
  });
  ipcMain.on('newPage', function (e, args) {
    if (args.type == 'jsonFormat') {
      const modalPath = process.env.NODE_ENV === 'development' ? 'http://localhost:9080/#/jsonFormat' : `file://${__dirname}/index.html#jsonFormat`;
      if (!newPageWin.jsonEditorWin) {
        newPageWin.jsonEditorWin = new BrowserWindow({
          minWidth: 1024,
          minHeight: 724,
          autoHideMenuBar: true,
          webPreferences: {
            webSecurity: false,
            devTools: true
          }
        });
        newPageWin.jsonEditorWin.loadURL(modalPath);
      } else {
        newPageWin.jsonEditorWin.focus();
        newPageWin.jsonEditorWin.webContents.send('jsonFormat', args.content);
      }
      newPageWin.jsonEditorWin.once('ready-to-show', () => {
        newPageWin.jsonEditorWin.webContents.send('jsonFormat', args.content);
        newPageWin.jsonEditorWin.show();
      });
      newPageWin.jsonEditorWin.on('close', function () {
        newPageWin.jsonEditorWin = null;
      });
    }
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    endWifi();
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
