import { dialog, Menu } from 'electron';
import { getWifiInfo } from './apicloud';

let fs = require('fs-extra');

let mainWindow;

//接收主进程的mainWindow
export function initMenu (obj) {
  mainWindow = obj;
  const menu = Menu.buildFromTemplate(applicationMenu);
  Menu.setApplicationMenu(menu);
}

export const applicationMenu = [
  {
    label: '文件',
    submenu: [
      {
        label: '新建',
        submenu: [
          {
            label: '文件',
            accelerator: 'CommandOrControl+N'
          },
          {type: 'separator'},
          {label: 'APICloud 模板文件'},
          {label: 'APICloud 页面框架'},
          {label: 'APICloud 移动应用'}]
      },
      {type: 'separator'},
      {
        label: '打开文件夹',
        click: openDirectory
      },
      {type: 'separator'},
      {
        label: '设置',
        click: setting
      },
      {label: '关闭窗口'}]
  },
  {
    label: '查看',
    submenu: [
      {
        label: '切换调试区域',
        accelerator: 'CommandOrControl+Shift+I'
      },
      {type: 'separator'},
      {
        label: '切换全屏',
        role: 'togglefullscreen',
        accelerator: 'F11'
      }]
  },
  {
    label: '窗口',
    role: 'window',
    submenu: [
      {
        label: '最小化',
        role: 'minimize'
      },
      {
        label: '关闭',
        role: 'close'
      },
      {
        label: '强制重启窗口',
        role: 'forcereload'
      }]
  },
  {
    label: '帮助',
    submenu: [
      {
        label: '文档',
        click () {
          require('electron').shell.openExternal('https://docs.apicloud.com/');
        }
      },
      {
        label: 'WIFI 真机同步IP和端口',
        click: getInfo
      },
      {
        label: '关于',
        click: about
      }]
  }];

function openDirectory () {
  dialog.showOpenDialog({
    properties: ['openDirectory']
  }, function (files) {
    if (files) {
      mainWindow.webContents.send('main', {
        action: 'openDirectory',
        files: files[0]
      });
    }
    
  });
  
}

function setting () {

}

function getInfo () {
  mainWindow.webContents.send('all-in', {
    type: 1,
    msg: getWifiInfo()
  });
}

function about () {
  mainWindow.webContents.send('all-in', {type: 3});
}

