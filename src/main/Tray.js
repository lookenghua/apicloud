const {app, Menu, Tray} = require('electron');
const path = require('path');
let mainWindow;
let tray = null;
let contextMenu;

export function initTray (win) {
  mainWindow = win;
  createTray();
}

export function createTray () {
  tray = new Tray(path.join(__dirname + '/icon.png'));
  contextMenu = Menu.buildFromTemplate([
    {
      label: '退出',
      click:function () {
        
      }
    }]);
  tray.setToolTip('APICloud');
  tray.setContextMenu(contextMenu);
  tray.on('double-click', () => {
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show()
  })
}

